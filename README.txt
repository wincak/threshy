Threshy

Threshy is a simple program for ThinkPad battery threshold configuration.
It uses TLP and basically mirrors it's interface to a system D-Bus service so
it is accessible without root privileges.

Threshy only controls current thresholds. Values are overwritten on reboot by
TLP-configured values.

Current D-Bus interface is rather experimental and prone to change.

Methods:
int getStartThreshold(int battery)
int setStartThreshold(int battery, int threshold)
- returns/sets battery start charge threshold - at what charge percentage
the battery starts charging.

int getStopThreshold(int battery)
int setStopThreshold(int battery)
- returns/sets battery stop charge threshold - at what charge percentage
the battery stops charing.

int fullCharge(int battery)
- overrides set thresholds and starts charging the battery to default values.

Parameters:
battery
- 1 for main battery, 2 for auxiliary
threshold
- 0 for default, 1-99 for percentage

Return values:
getter methods return requested thresholds
setter methods return 0 on success or error code

Known issues:
getter methods return 0 for default values
- So if for example your default start charge is 96%, the method returns 0.
using secondary battery has not been tested

Usage:
There is a simple Qt frontend Threshy-gui. If that does not fit your needs,
feel free to use the D-Bus interface directly.

Threshy is licensed under GPLv3.

