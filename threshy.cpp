#include "threshy.h"

#include "errno.h"

#include <QStringList>

#include "definitions.h"
#include "threshyadaptor.h"

/*
   To connect to system bus some policy files are needed as well
   Service file: /usr/share/dbus-1/system-services/org.wincak.threshy.service
   Policy file: /etc/dbus-1/system.d/org.wincak.threshy.conf
*/

Threshy::Threshy(QObject* parent)
    : QObject(parent)
{
    new ThreshyAdaptor(this);

    QDBusConnection dbus = QDBusConnection::systemBus();
    if (!dbus.isConnected()) {
        qFatal("Failed to connect to D-Bus");
    }

    bool success;
    success = dbus.registerService("org.wincak.threshy");
    if (!success) {
        qFatal("Failed to register service");
    }

    success = dbus.registerObject("/org/wincak/threshy", this);
    if (!success) {
        qFatal("Failed to register object");
    }
}

Threshy::~Threshy()
{
}

int Threshy::getStartThreshold(int battery)
{
    if ((battery < 1) || (battery > 2)) {
        return (-EINVAL);
    }

    QStringList arguments;
    arguments << QString(CMD_GET);
    arguments << QString(ARG_START_THRESHOLD);
    arguments << QString::number(battery);

    QString reply;
    QString error;
    int ret = callTPAcpi(arguments, &reply, &error);
    if (ret != EXIT_SUCCESS) {
        qCritical("%s", error.toLocal8Bit().data());
        return (ret);
    }

    // reply should be in format "75 (relative percent)", so we need just the first part
    QStringList replyWords = reply.split(" ");

    bool replyOk;
    int thresholdValue = replyWords.first().toInt(&replyOk);
    if (replyOk == false) {
        return (INVALID_THRESHOLD);
    }

    return (thresholdValue);
}

int Threshy::setStartThreshold(int battery, int threshold)
{
    if ((battery < 1) || (battery > 2)) {
        return (-EINVAL);
    }
    if ((threshold < 0) || (threshold > 99)) {
        return (-EINVAL);
    }

    QStringList arguments;
    arguments << QString(CMD_SET);
    arguments << QString(ARG_START_THRESHOLD);
    arguments << QString::number(battery);
    arguments << QString::number(threshold);

    QString reply;
    QString error;
    int ret = callTPAcpi(arguments, &reply, &error);
    if (ret != EXIT_SUCCESS) {
        qCritical("%s", error.toLocal8Bit().data());
        return (ret);
    }

    // There should be no reply. Any reply is error string
    if (!reply.isEmpty()) {
        // Warning has already been printed by callExternal()
        return (-EIO);
    }

    return (EXIT_SUCCESS);
}

int Threshy::getStopThreshold(int battery)
{
    if ((battery < 1) || (battery > 2)) {
        return (-EINVAL);
    }

    QStringList arguments;
    arguments << QString(CMD_GET);
    arguments << QString(ARG_STOP_THRESHOLD);
    arguments << QString::number(battery);

    QString reply;
    QString error;
    int ret = callTPAcpi(arguments, &reply, &error);
    if (ret != EXIT_SUCCESS) {
        qCritical("%s", error.toLocal8Bit().data());
        return (ret);
    }

    // reply should be in format "75 (relative percent)", so we need just the first part
    QStringList replyWords = reply.split(" ");

    bool replyOk;
    int thresholdValue = replyWords.first().toInt(&replyOk);
    if (replyOk == false) {
        return (INVALID_THRESHOLD);
    }

    return (thresholdValue);
}

int Threshy::setStopThreshold(int battery, int threshold)
{
    if ((battery < 1) || (battery > 2)) {
        return (-EINVAL);
    }
    if ((threshold < 0) || (threshold > 99)) {
        return (-EINVAL);
    }

    QStringList arguments;
    arguments << QString(CMD_SET);
    arguments << QString(ARG_STOP_THRESHOLD);
    arguments << QString::number(battery);
    arguments << QString::number(threshold);

    QString reply;
    QString error;
    int ret = callTPAcpi(arguments, &reply, &error);
    if (ret != EXIT_SUCCESS) {
        qCritical("%s", error.toLocal8Bit().data());
        return (ret);
    }

    // There should be no reply. Any reply is error string
    if (!reply.isEmpty()) {
        // Warning has already been printed by callExternal()
        return (-EIO);
    }

    return (EXIT_SUCCESS);
}

bool Threshy::getInhibitCharge(int battery)
{
    if ((battery < 0) || (battery > 2)) {
        qCritical("%s invalid battery index %i", __PRETTY_FUNCTION__, battery);
        return (false);
    }

    QStringList arguments;
    arguments << QString(CMD_GET);
    arguments << QString(ARG_INHIBIT_CHARGE);
    arguments << QString::number(battery);

    QString reply;
    QString error;
    int ret = callTPAcpi(arguments, &reply, &error);
    if (ret != EXIT_SUCCESS) {
        qCritical("%s", error.toLocal8Bit().data());
        return (ret);
    }

    // reply should be in format "yes" or "no"
    bool inhibitChargeSet = false;
    bool replyOk = false;
    if (reply.compare("yes") == 0) {
        inhibitChargeSet = true;
        replyOk = true;
    } else if (reply.compare("no") == 0) {
        inhibitChargeSet = false;
        replyOk = true;
    }

    if (!replyOk) {
        qCritical("%s unknown reply: %s", __PRETTY_FUNCTION__, reply.toLocal8Bit().data());
        return (false);
    }

    return (inhibitChargeSet);
}

int Threshy::fullCharge(int battery)
{
    if ((battery != 0) && (battery != 1)) {
        return (-EINVAL);
    }

    int ret;
    ret = setStartThreshold(battery, 0);
    if (ret) {
        return (ret);
    }
    ret = setStopThreshold(battery, 0);
    if (ret) {
        return (ret);
    }

    return (EXIT_SUCCESS);
}

int Threshy::callTPAcpi(QStringList arguments, QString* pOutput, QString* pError)
{
    if (arguments.isEmpty()) {
        if (pError) {
            *pError = tr("Empty argument list");
        }
        return (-EINVAL);
    }

    QProcess process;
    process.start(TPACPI_BAT_PATH, arguments);

    bool success = process.waitForFinished(EXTERNAL_CALL_TIMEOUT_MS);
    if (success == false) {
        if (pError) {
            *pError = tr("Error calling tpacpi-bat: %s").arg(process.errorString().toLocal8Bit().data());
        }
        return (-EIO);
    }

    if (pOutput) {
        QByteArray output = process.readAllStandardOutput();

        // Cutting newlines
        *pOutput = QString(output).simplified();
    }

    return (EXIT_SUCCESS);
}
