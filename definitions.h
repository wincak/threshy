#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define TPACPI_BAT_PATH "/usr/share/tlp/tpacpi-bat"

#define EXTERNAL_CALL_TIMEOUT_MS    1000

#define INVALID_THRESHOLD   -1

#define CMD_GET     "-g"
#define CMD_SET     "-s"

#define ARG_START_THRESHOLD "ST"
#define ARG_STOP_THRESHOLD  "SP"
#define ARG_INHIBIT_CHARGE  "IC"
#define ARG_FORCE_DISCHARGE "FD"

// According to tpacpi-bat (TLP) help, peak shift state is "mysterious"
#define ARG_PEAK_SHIFT_STATE    "PS"

#endif // DEFINITIONS_H
