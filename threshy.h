#ifndef THRESHY_H
#define THRESHY_H

#include <QObject>

class Threshy : public QObject {
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.wincak.threshy")

public:
    explicit Threshy(QObject* parent = nullptr);
    ~Threshy();

public slots:
    Q_SCRIPTABLE int getStartThreshold(int battery);
    Q_SCRIPTABLE int setStartThreshold(int battery, int threshold);

    Q_SCRIPTABLE int getStopThreshold(int battery);
    Q_SCRIPTABLE int setStopThreshold(int battery, int threshold);

    Q_SCRIPTABLE bool getInhibitCharge(int battery);

    Q_SCRIPTABLE int fullCharge(int battery);

private:
    int callTPAcpi(QStringList arguments, QString* pOutput, QString* pError);
};

#endif // THRESHY_H
