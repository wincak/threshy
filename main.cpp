#include <QCoreApplication>

#include <QtDBus/QtDBus>

#include "definitions.h"
#include "threshy.h"

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    Threshy s(&app);

    app.exec();
    return (0);
}
